﻿import React, { Component } from 'react';
import { Panel } from 'react-bootstrap';


export default class TemplateLayer extends Component {

	render() {
		return (
			<div className="App">
				<header className="App-header">
					{this.props.header}
				</header>
				<Panel className="App-content">
					{this.props.content}
				</Panel>
				<footer className="App-footer">
					{this.props.footer}
				</footer>
			</div>
		);
	}
}



import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Tab, Row, Nav, NavItem, Col } from 'react-bootstrap';

export class LoginRegisterTab extends Component {
    
	render() {
		return (
			<Tab.Container id="tabs-default" defaultActiveKey="first">
				<Row className="clearfix">
					<Col sm={12}>
						<Nav bsStyle="tabs">
							<NavItem eventKey="first" href="/login">
								Login
							</NavItem>
							<NavItem eventKey="second" href="/signup">
								Register
							</NavItem>
							<NavItem eventKey="third" href="/form">
								Form
							</NavItem>
						</Nav>
					</Col>
					<Col sm={12}>
						<Tab.Content animation>
							<Tab.Pane eventKey="first">
								<Col sm={2}></Col>
								<Col sm={8}>
									{this.props.login}
								</Col>
								<Col sm={2}></Col>
							</Tab.Pane>
							<Tab.Pane eventKey="second">
								<Col sm={2}></Col>
								<Col sm={8}>
									{this.props.register}
								</Col>
								<Col sm={2}></Col>
							</Tab.Pane>
							<Tab.Pane eventKey="third">
								<Col sm={2}></Col>
								<Col sm={8}>
									{this.props.form}
								</Col>
								<Col sm={2}></Col>
							</Tab.Pane>
						</Tab.Content>
					</Col>
				</Row>
			</Tab.Container>
		);
	}
}
    
    
export class UserTab extends Component {
    
	render() {
		return (
			<Tab.Container id="tabs-user" defaultActiveKey="first">
				<Row className="clearfix">
					<Col sm={12}>
						<Nav bsStyle="tabs">
							<NavItem eventKey="first">
                                    ข้อมูลส่วนตัว
							</NavItem>
							<NavItem eventKey="second">
                                    ยื่นคำร้องขอใบรับรอง
							</NavItem>
						</Nav>
					</Col>
					<Col sm={12}>
						<Tab.Content animation>
							<Tab.Pane eventKey="first">
								<Col sm={2}></Col>
								<Col sm={8}>
                                        Profile of {this.props.user}
								</Col>
								<Col sm={2}></Col>
							</Tab.Pane>
							<Tab.Pane eventKey="second">
								<Col sm={2}></Col>
								<Col sm={8}>
                                        Request Form
								</Col>
								<Col sm={2}></Col>
							</Tab.Pane>
						</Tab.Content>
					</Col>
				</Row>
			</Tab.Container>
		);
	}
}
    
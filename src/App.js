import React, { Component} from 'react';
import {  BrowserRouter as Router,
	Route,
	Link } from 'react-router-dom';
import './App.css';
import FormEvaluation from './comps/FormContent.js';
import { FormRequest } from './comps/RequestContent.js';
import { FormRegister } from './comps/RegisterContent.js';
import { FormLogin } from './comps/LoginContent.js';
import { polyfill } from 'es6-promise'; 
polyfill();


const evaluation_fields = [
	{ 
		id: 1,
		order: '1',
		no: 'AF พื้นฐานด้านฟาร์ม(AF)',
		group1: 'AF.1 ประวัติพื้นที่และการจัดการพื้นที่',
		group2: 'AF.1.1 ประวัติพื้นที่',
		group3: 'AF.1.1.',
		control_point_group: 
		[
			{ 
				id: 1, 
				control_point: 'สำหรับแปลง สวน หรือโรงเรียน หรือพื้นที่ที่ใช้สำหรับการผลิตมีระบบอ้างอิงที่นำไปอ้างไว่ในแผนที่ฟาร์มหรือไม่',
				criteria: 'มีระบบการอ้างอิงของแต่ละแปลงผลิตและนำไปอ้างอิงในแผนผังแปลง',
				level: 'Minor Must',
				status: null,
				note: null,
				editor: null,
				choices: [],
				fields: []
			},
			{ 
				id: 2, 
				control_point: 'มีระบบการบันทึกที่เกี่ยวกับการผลิตสำหรับทุกหน่วยการผลิตหรือไม่',
				criteria: 'ต้องจัดเก็บบันทึกต่างๆไว้เป็นหลักฐานในระยะเวลาที่เหมาะสม',
				level: 'Majot Must',
				status: null,
				note: null,
				editor: null,
				choices: [],
				fields: []
			}
		]
	},
	{ 
		id: 2,
		order: '2',
		no: 'AF พื้นฐานด้านฟาร์ม(AF)',
		group1: 'AF.2 ประวัติพื้นที่และการจัดการพื้นที่',
		group2: 'AF.2.1 ประวัติพื้นที่',
		group3: 'AF.2.1.',
		control_point_group: 
		[
			{ 
				id: 1, 
				control_point: 'สำหรับแปลง สวน หรือโรงเรียน หรือพื้นที่ที่ใช้สำหรับการผลิตมีระบบอ้างอิงที่นำไปอ้างไว่ในแผนที่ฟาร์มหรือไม่',
				criteria: 'มีระบบการอ้างอิงของแต่ละแปลงผลิตและนำไปอ้างอิงในแผนผังแปลง',
				level: 'Minor Must',
				status: null,
				note: 'risk-form',
				editor: null,
				choices: [],
				fields: []
			},
			{ 	
				id: 2, 
				control_point: 'มีระบบการบันทึกที่เกี่ยวกับการผลิตสำหรับทุกหน่วยการผลิตหรือไม่',
				criteria: 'ต้องจัดเก็บบันทึกต่างๆไว้เป็นหลักฐานในระยะเวลาที่เหมาะสม',
				level: 'Majot Must',
				status: null,
				note: null,
				editor: null,
				choices: [],
				fields: []
			}
		]
	},
	{ 
		id: 3,
		order: '3',
		no: 'AF พื้นฐานด้านฟาร์ม(AF)',
		group1: 'AF.3 ประวัติพื้นที่และการจัดการพื้นที่',
		group2: 'AF.3.1 ประวัติพื้นที่',
		group3: 'AF.3.1.',
		control_point_group: 
		[
			{ 
				id: 1, 
				control_point: 'สำหรับแปลง สวน หรือโรงเรียน หรือพื้นที่ที่ใช้สำหรับการผลิตมีระบบอ้างอิงที่นำไปอ้างไว่ในแผนที่ฟาร์มหรือไม่',
				criteria: 'มีระบบการอ้างอิงของแต่ละแปลงผลิตและนำไปอ้างอิงในแผนผังแปลง',
				level: 'Minor Must',
				status: null,
				note: 'risk-form',
				editor: null,
				choices: [],
				fields: []
			},
			{ 
				id: 2, 
				control_point: 'มีระบบการบันทึกที่เกี่ยวกับการผลิตสำหรับทุกหน่วยการผลิตหรือไม่',
				criteria: 'ต้องจัดเก็บบันทึกต่างๆไว้เป็นหลักฐานในระยะเวลาที่เหมาะสม',
				level: 'Majot Must',
				status: null,
				note: null,
				editor: null,
				choices: [],
				fields: []
			}
		]
	}
];


const Home = () => (
	<div>
		<h2>Home</h2>
		<FormLogin />
	</div>
);
  
const Register = () => (
	<div>
		<h2>Register</h2>
		<FormRegister />
	</div>
);

const Request = () => (
	<div>
		<h2>Request</h2>
		<FormRequest />
	</div>
);
  
const Forms = ({ match }) => (
	<div>
		<h2>Forms</h2>
		<ul>
			<li>
				<Link to={`${match.url}/F05`}>
				Form 05
				</Link>
			</li>
			<li>
				<Link to={`${match.url}/F01`}>
				Form 01
				</Link>
			</li>
			<li>
				<Link to={`${match.url}/F02`}>
				Form 03
				</Link>
			</li>
		</ul>
  
		<Route path={`${match.url}/:topicId`} component={Form}/>
		<Route exact path={match.url} render={() => (
			<h3>Please select a form.</h3>
		)}/>
	</div>
);
  
const Form = ({ match }) => (
	<FormEvaluation name="gapThaiForm" status={['yes','no']} note={['note','file']} field={evaluation_fields} />

);

class App extends Component {

	constructor(){
		super();
		this.state = {
			speed: 10
		};
	}

	componentDidMount(){
		fetch('http://158.108.181.116/gap/apiv1/action-type/1')
			.then(results => {
				return results.json();
			})
			.then(data =>  {
				let pictures = data.results;
				this.setState({pictures: pictures});
				console.log('state', this.state.pictures);
			});


		fetch('http://158.108.39.53/gap/apiv1/auth/'+this.state.activity, {
			method: 'POST',
			//make sure to serialize your JSON body
			body: JSON.stringify({
				email: 'sasa@dsc.cds',
				password: 'adsvdsvds2dc'
			})
		}).then((res) => res.json())
			.then((data) =>  console.log(this.state.activity+' complete'))
			.catch((err)=>console.log(err));
				
	}

	render() {
		return (
			<Router>
				<div>
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/register">Register</Link></li>
						<li><Link to="/request">Request</Link></li>
						<li><Link to="/forms">Forms</Link></li>
					</ul>
					<hr/>

					<Route exact path="/" component={Home}/>
					<Route path="/register" component={Register}/>
					<Route path="/request" component={Request}/>
					<Route path="/forms" component={Forms}/>
				</div>
			</Router>

		);
	}
}

export default App;

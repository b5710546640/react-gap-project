import React from 'react';
import ReactDOM from 'react-dom';
import {  Router, Route, hashHistory } from 'react-router';
import './index.css';
import App, { Login,Register,Evaluation } from './App';

import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import * as firebase from 'firebase';




var config = {
	apiKey: 'AIzaSyD8q9lyxwKzJ5yxetGlSLKKbj-XrtqefaY',
	authDomain: 'gap-web.firebaseapp.com',
	databaseURL: 'https://gap-web.firebaseio.com',
	projectId: 'gap-web',
	storageBucket: '',
	messagingSenderId: '537664740396'
};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));

registerServiceWorker();

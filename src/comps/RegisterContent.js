import React, { Component } from 'react';
import Validation from '../validations/Validation.js';

export class FormRegister extends Component {
	render() {
		return (
			<Validation
				activity='register'
				inputs={
					[
						{ id: 1, name: 'รูปแบบสมาชิก', type: 'radio', require: true, rule: 'กรุณาระบุรูปแบบใบรับรองที่ต้องการ', display: 'input'  },
						{ id: 2, name: 'email', type: 'email', require: true, rule: 'อีเมล์ของท่านไม่ถูกต้อง', display: 'input' },
						{ id: 3, name: 'password', type: 'confirmPassword', require: true, rule: 'รหัสผ่านไม่ตรงกัน', display : 'input'  },
						{ id: 4, name: 'ชื่อ-สกุล', type: 'name', require: true, rule: 'ห้ามเว้นว่าง', display: 'input'  },
						{ id: 5, name: 'idcard', type: 'idcard', require: true, rule: 'เลขประจำตัวประชาชน13หลัก', display: 'input'  },
						{ id: 6, name: 'ที่อยู่', type: 'address', require: true, rule: 'ระบุที่อยู่ของท่าน', display: 'input'  },
						{ id: 7, name: 'signup', type: 'submit', require: true, rule: '', display: 'input'  }
					]
				}
			/>);
	}
}
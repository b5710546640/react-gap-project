﻿import React, { Component } from 'react';

export class HeaderPanel extends Component {

	render() {
		return (
			<h1 className="App-title">ระบบลงทะเบียนขอใบรับรอง ThaiGAP{this.props.speed}</h1>
		);
	}
}

export class FooterPanel extends Component {

	render() {
		return (
			<small className="App-title">Certificate Body</small>
		);
	}
}


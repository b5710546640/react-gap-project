import React, { Component } from 'react';
import Validation from '../validations/Validation.js';
import Progress from 'react-progressbar';
import { Col,Row, Table, Button, Checkbox } from 'react-bootstrap';


export default class FormEvaluation extends Component {

	constructor(props) {
		super(props);
		this.state = {
			process: 0
		};
	}

	handleData(event){
		this.setState({
			process: event
		});
	}

	render() {
		return (
			<div className="App">
				<div>
					<Row>{this.props.field.length}</Row>
					<Row><Progress completed={this.state.process} /></Row>
				</div>	
				<Validation 
					onData={this.handleData.bind(this)}
					name={this.props.name}
					status={this.props.status}
					note={this.props.note}
					activity='show'
					inputs={this.props.field}
					onSubmit={this.props.onSubmit}
				/>
			</div>
		);
	}
}
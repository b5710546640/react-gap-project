import React, { Component } from 'react';
import Validation from '../validations/Validation.js';

export class FormLogin extends Component {
	render() {
		return (
			<Validation
				activity='login'
				inputs={
					[
						{ id: 1, name: 'email', type: 'email', require: true, rule: 'อีเมล์ของท่านไม่ถูกต้อง', display: 'input' },
						{ id: 2, name: 'password', type: 'password', require: true, rule: 'กรุณาตรวจสอบรหัสผ่าน', display: 'input'  },
						{ id: 3, name: 'signin', type: 'submit', require: true, rule: '', display: 'input'  }
					]
				}
			/>);
	}
}
import React, { Component } from 'react';
import Validation from '../validations/Validation.js';

export class FormRequest extends Component {
	render() {
		return (
			<Validation
				activity='request'
				inputs={
					[
						{ id: 1, name: 'เลขประจำตัวประชาชน', type: 'search-user', require: true, rule: 'เลขประจำตัวประชาชน13หลัก', display: 'input'  },
						{ id: 2, name: 'รายชื่อแปลง', type: 'farm', require: true, rule: 'กรุณาระบุอย่างน้อยหนึ่งอย่าง', display: 'input'  },
						{ id: 3, name: 'submit', type: 'submit', require: true, rule: '', display: 'input'  }
					]
				}
			/>);
	}
}
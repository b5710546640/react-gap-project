import React from 'react';
import '../../App.css';
import { ValidationInput, ValidationRadio } from '../component/input/ValidationInput.js';
import ValidationAddress from '../component/input/groupInput/ValidationAddress.js';
import ValidationConfirmPassword from '../component/input/groupInput/ValidationConfirmPassword.js';
import ValidationFarmField from '../component/input/groupInput/ValidationFarmField.js';
import ValidationSearchUser from '../component/input/groupInput/ValidationSearchUser.js';
import ValidationName from '../component/input/groupInput/ValidationName.js';
import ValidationButton from '../component/ValidationButton.js';
import { Col,Row, Button, Checkbox } from 'react-bootstrap';
import * as firebase from 'firebase';
import fetch from 'isomorphic-fetch';
import { polyfill } from 'es6-promise'; 

import FormEvaluation from '../../comps/FormContent.js';

polyfill();

let stack = [];
var self;
export default class ValidationForm extends React.Component {



	constructor(props) {
		super(props);
		this.state = {
			formname: props.formname,
			comps: props.comps,
			results: props.results,
			activity: props.activity,
			openSubForm: 0,
			process: 0,
			count: 0,
			datas: {},
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleFormRisk = this.handleFormRisk.bind(this);
		self = this;
	}

	writeUserData(email,pass) {
		firebase.database().ref('users/').set({
			email: email,
			password: pass
		});
	}

	handleFormRisk(event) {
		console.log(event.target.value);
		this.setState({ openSubForm: event.target.value });
	}

	
	showGroupForm(title,index, order, no, group1, group2, group3, control_point_groups) {
		let table = [];
		
		console.log('group2 :'+group2);
		
		if(order != null){
			table.push(
				<Row  key="row-group1" id="row-group1" className="border-table">
					<Col md={1}>{order}</Col>
					<Col md={1}></Col>
					<Col md={2}></Col>
					<Col md={2}></Col>
					<Col md={1}></Col>
					<Col md={2}>
						<Row>
							<Col md={6}></Col>
							<Col md={6}></Col>
						</Row>
					</Col>
					<Col md={2}></Col>
					<Col md={1}></Col>
				</Row>
			);
		}
		if(group2 != null){
			table.push(
				<Row id="row-group2" key="row-group2" className="border-table">
					<Col md={1}></Col>
					<Col md={1}>{group2}</Col>
					<Col md={2}></Col>
					<Col md={2}></Col>
					<Col md={1}></Col>
					<Col md={2}>
						<Row>
							<Col md={6}></Col>
							<Col md={6}></Col>
						</Row>
					</Col>
					<Col md={2}></Col>
					<Col md={1}></Col>
				</Row>
			);
		}
		if(group3 != null){
			table.push(
				control_point_groups.map((control_point) =>
					<Row id={'row-group3-'+control_point.id} key={'row-group3-'+control_point.id} className="border-table" key={control_point.id}>
						<Col md={1}></Col>
						<Col md={1}>{group3+control_point.id}</Col>
						<Col md={2}>
							<Row>{control_point.control_point}</Row>
							<Row>
								{
									control_point.choices.length > 0 ? 
										<fieldset id={'ch-'+group3+'uuu'+control_point.id}>
											{control_point.choices.map(function(choice, index){
												return 	(
													<Checkbox  className="show-grid" key={'ch-'+group3+control_point.id+index}>
														{choice}
													</Checkbox>
												);
											})}
										</fieldset>
										: ''
								}
							</Row>
							<Row>
								{
									control_point.fields.length > 0 ? 
										<Row>
											{control_point.fields.map(function(field,index) {
												return (<Row  className="show-grid" key={'fd-'+group3+control_point.id+index}>
													<ValidationInput handleData={self.handleData.bind(this)} value='' name={field} type="text"/>
												</Row>);
											})}
										</Row>
										: '' 
								}
							</Row>
						
						</Col>
						<Col md={2}>{control_point.criteria}</Col>
						<Col md={1}>{control_point.level}</Col>

						{(() => {
							switch (this.state.formname) {
							case 'RiskEvaluation':
								return (
									<ValidationRadio handleData={self.handleData.bind(this)} name='status' formname={this.state.formname} tag={group3+control_point.id} />
								);
							default:
								return (
									<div>
										<Col md={2}>
											<ValidationRadio handleData={self.handleData.bind(this)} name='status' tag={group3+control_point.id} />
										</Col>
										<Col md={2}>
											{(() => {
												switch (control_point.note) {
												case 'risk-form':
													return (
														<Button type="button" onClick={this.handleFormRisk}>
													ประเมินความเสี่ยง
														</Button>
													);
												default:
													return <ValidationInput handleData={self.handleData.bind(this)} value='' name={this.state.formname} type={this.props.note[1]} src={control_point.note}/>;
												}
											})()}
										</Col>
									</div>
								);
							}
						})()}

						<Col md={1}><ValidationInput handleData={self.handleData.bind(this)} value='' type="text"/></Col>
					</Row>
				)

			);
		}

		return table;
	}

	selectType(index, type, name, isRequired, rule, control_point_groups) {
		console.log(control_point_groups);
		if (type === 'submit') return <ValidationButton handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		else if (type === 'confirmPassword') return <ValidationConfirmPassword handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		else if (type === 'address') return <ValidationAddress handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		else if (type === 'name') return <ValidationName handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		else if (type === 'radio') return <ValidationRadio handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		else if (type === 'farm') return <ValidationFarmField handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		else if (type === 'search-user') return <ValidationSearchUser handleData={self.handleData.bind(this)} name={name} type={type} require={isRequired} rule={rule} />;
		return <ValidationInput handleData={self.handleData.bind(this)} name={name} type={type} require={false} rule={rule} />;
	}

	
	handleSubmit(event) {
		console.log('Submit in Validation', this.state.comps);
		alert('A name was submitted: ' + this.input.value);
		this.writeUserData(this.refs.form.email.value, this.refs.form.password.value);
		console.log('AAAAAAAAAAA ' + [event.target.name] + ' ' + event.target.type);

		fetch('http://158.108.39.53/gap/apiv1/auth/'+this.state.activity, {
			method: 'POST',
			//make sure to serialize your JSON body
			body: JSON.stringify({
				email: 'sasa@dsc.cds',
				password: 'adsvdsvds2dc'
			})
		}).then((res) => res.json())
			.then((data) =>  console.log(this.state.activity+' complete'))
			.catch((err)=>console.log(err));
		

		event.preventDefault();
	}



	generateTable(input){
		let table = [];

		for(const post of input){

			if (post.id === 1){
				table.push(
					<Row key="header-table" id="header-table" className="border-table">
						<Col md={1}>Order</Col>
						<Col md={1}>Group</Col>
						<Col md={2}>Control Point</Col>
						<Col md={2}>Criteria</Col>
						<Col md={1}>Level</Col>
						<Col md={2}>
							<Row>Status</Row>
							<Row>
								<Col md={6}>{this.props.status[0]}</Col>
								<Col md={6}>{this.props.status[1]}</Col>
							</Row>
						</Col>
						<Col md={2}>{this.props.note[0]}</Col>
						<Col md={1}>Editor</Col>
					</Row>
				);
				table.push(this.showGroupForm(this.props.status,post.id, post.order, post.no, post.group1, post.group2, post.group3, post.control_point_group));
			}else{
				table.push(this.showGroupForm(this.props.status,post.id, post.order, post.no, post.group1, post.group2, post.group3, post.control_point_group));
			}

		}
		return table;
	}


	handleData(key,value,valid){
		console.log(valid+'at '+key+': '+value);
		if(valid){
			if(stack.indexOf(key)<0)
				stack.push(key);
			this.setState({
				process: stack.length
			});
			console.log(stack);
		}

	}

	render() {

		if(this.state.activity === 'show' ){
			if(this.state.openSubForm !== 0)
				return (
					<FormEvaluation 
						name="RiskEvaluation"
						status={['ผ่าน','เสี่ยง']}
						note={['แนวทางแก้ไข','text']}
						field={
							[
								{	
									id: 1,
									order: '1',
									no: 'พื้นที่ปลูก',
									group1: null,
									group2: null,
									group3: 'SD.1.',
									control_point_group: 
									[
										{ id: 1, 
											control_point: 'แปลงปลูกเป็นพื้นที่เพาะปลูกอย่างต่อเนื่องมาจนถึงปัจจุบันหรือไม่',
											criteria: '',
											level: '',
											status: null,
											note: null,
											editor: null,
											choices: [],
											fields: []
										},
										{ id: 2, 
											control_point: 'แปลงปลูกเคยวิเคราะหฺตรวจสอบดินหรือไม่',
											criteria: '',
											level: '',
											status: null,
											note: null,
											editor: null,
											choices: [],
											fields: []
										},
										{ id: 3, 
											control_point: 'แปลงปลูกเคยเป็นสถานที่ทิ้งขยะหรือแหล่งทิ้งของอันตรายหรือไม่',
											criteria: '',
											level: '',
											status: null,
											note: null,
											editor: null,
											choices: ['ขยะมูลฝอยทั่วไป','เศษซากพืชจากการเพาะปลูก','ขยะอันตราย ขยะปนเปื้อนโลหะหนัก','บรรจุภัณฑ์ทางการเกษตร เช่น กระสอบปุ๋ย, ถุงปุ๋ย, ขวดสารเคมี'],
											fields: []

										},
										{ id: 4, 
											control_point: 'มีแปลงเพาะปลูฏพืชชนิดอื่นอยู่ในบิเวณใกล่เคียงหรือไม่',
											criteria: '',
											level: '',
											status: null,
											note: null,
											editor: null,
											choices: [],
											fields: ['ระยะห่าง','ชนิดพืช']
										}
									]
								},
								{	
									id: 2,
									order: '2',
									no: 'แหล่งน้ำ',
									group1: null,
									group2: null,
									group3: 'SD.2.',
									control_point_group: 
									[
										{ id: 1, 
											control_point: 'แหล่งน้ำที่ใช่น้ำไหลผ่านที่ชุมชนหรือไม่',
											criteria: '',
											level: '',
											status: null,
											note: null,
											editor: null,
											choices: [],
											fields: []
										},
										{ id: 2, 
											control_point: 'แหล่งน้ำที่ใช้น้ำไหลผ่านคอกปศุสัตว์หรือไม่',
											criteria: '',
											level: '',
											status: null,
											note: null,
											editor: null,
											choices: [],
											fields: []
										}
									]
								}
							]
						}
						onSubmit={this.handleChange}/>
				);

			return (
				<div className="App">
					<form onSubmit={this.handleSubmit} ref="form">
						{this.generateTable(this.state.comps)}
						<ValidationButton name="Save" type="submit" onSubmit={this.props.onSubmit}/>
					</form>
				</div>
			);	
		}
		
		return (
			<div className="App">
				<form onSubmit={this.handleSubmit} ref="form">
					{this.state.comps.map((post) =>
						<Row  className="show-grid" key={post.id}>
							{this.selectType(post.id, post.type, post.name, post.require, post.rule, post.control_point_group)}
						</Row>
					)}
				</form>
			</div>
		);
	}
}


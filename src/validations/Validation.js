import React from 'react';
import ValidationForm from './form/ValidationForm.js';

export default class Validation extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			formname: props.name,
			activity: props.activity,
			array: props.inputs,
			results: []
		};
		console.log('FORM NAME:'+this.state.formname);

	}
    


	render() {
		return (
			<TemplateValidation
				onData={this.props.onData}
				formname={this.state.formname}
				status={this.props.status}
				note={this.props.note}
				activity={this.state.activity}
				posts={this.state.array}
				results={this.state.results}
			/>
		);
	}
}




class TemplateValidation extends React.Component {



	constructor(props) {
		super(props);
		this.state = {
			formname: props.formname,
			activity: props.activity,
			posts: props.posts,
			results: [],
			value: ''
		};
		this.handleChange = this.handleChange.bind(this);

	}




	handleChange(event) {
		this.setState({
			value: event.target.value
		});
		console.log('cd '+this.state.value);
	}

 



	render() {
		console.log('FORM NAME2:'+this.state.formname);
		return (
			<ValidationForm
				onData={this.props.onData}
				formname={this.state.formname}
				status={this.props.status}
				note={this.props.note}
				activity={this.state.activity}
				comps={this.state.posts}
				results={this.state.value}
				onSubmit={this.props.onSubmit}
			/>
		);
	}

}
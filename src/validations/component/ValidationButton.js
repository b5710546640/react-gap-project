import React from 'react';
import { Button } from 'react-bootstrap';

export default class ValidationButton extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			type: props.type
		};

	}

	render() {
		return (
			<Button type={this.state.type}>
				{this.state.title}
			</Button>
		);
	}
}
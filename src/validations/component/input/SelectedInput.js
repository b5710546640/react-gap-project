import React from 'react';
import '../../../App.css';
import { HelpBlock, FormGroup, FormControl, ControlLabel, Radio, Row, Col } from 'react-bootstrap';
import { ValidationInput } from './ValidationInput';

export class SelectedInput extends ValidationInput {
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			value: '',
			type: props.type,
			isRequire: props.require,
			rule: props.rule,
			validMsg: null,
			option: props.option
		};
		this.handleChange = this.handleChange.bind(this);
		console.log('Option',this.state.option);

	}

	componentDidMount(){
		this.setState({ isRequire: this.props.require });
		console.log('Require :'+this.state.isRequire);
	}

	getErrorMessage() {
		if (this.state.validMsg === '') return null;
		if (this.state.validMsg ==='success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}

	getValidationState() {
		return this.state.validMsg;
	}

	
	getValidationMessage(addr,val) {
		const len = val.length;
		if (len === 0) return null;
		if (addr === 'email') {
			if (val.indexOf('.') - val.indexOf('@') > 0) {
				if (val.indexOf('.') !== -1 && val.indexOf('.') <= (len - 2))
					return 'success';
			}
		} else if (addr === 'idcard') {
			console.log('length :'+len);
			if (len === 13) return 'success';
		} else if (len >= 5) return 'success';
		return 'error';
	}

	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success')
			this.props.handleData(nextState.type,nextState.value,nextState.validMsg==='success');
	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}

	handleChange(event) {
		let addr = event.target.name;
		let val = event.target.value;
		this.setState({
			value: val,
			validMsg: this.getValidationMessage(addr,val)
		});
		// this.handleData(val);
	}


	render() {
		return (
			<select
				className="form-control"
				name="prefix_name"
				onChange={this.handleChange}
			>
				{this.state.option.map((post) =>
					<option  value={post} key={post.id}>
						{post}
					</option>
				)}
			</select>
		);
	}
}


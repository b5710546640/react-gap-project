﻿import React from 'react';
import '../../../App.css';
import { HelpBlock, FormGroup, FormControl, ControlLabel, Radio, Row, Col } from 'react-bootstrap';

export class ValidationInput extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			value: '',
			type: props.type,
			isRequire: props.require,
			rule: props.rule,
			validMsg: null
		};

	}

	componentDidMount(){
		this.setState({ isRequire: this.props.require });
		console.log('Require :'+this.state.isRequire);
	}

	getErrorMessage() {
		if (this.state.validMsg === '') return null;
		if (this.state.validMsg ==='success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}

	getValidationState() {
		return this.state.validMsg;
	}

	
	getValidationMessage(addr,val) {
		const len = val.length;
		if (len === 0) return null;
		if (addr === 'email') {
			if (val.indexOf('.') - val.indexOf('@') > 0) {
				if (val.indexOf('.') !== -1 && val.indexOf('.') <= (len - 2))
					return 'success';
			}
		} else if (addr === 'idcard') {
			console.log('length :'+len);
			if (len === 13) return 'success';
		} else if (len >= 5) return 'success';
		return 'error';
	}

	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success')
			this.props.handleData(nextState.type,nextState.value,nextState.validMsg==='success');
	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}

	handleChange(event) {
		let addr = event.target.name;
		let val = event.target.value;
		this.setState({
			value: val,
			validMsg: this.getValidationMessage(addr,val)
		});
		// this.handleData(val);
	}


	render() {
		return (
			<FormGroup
				ref="form"
				controlId="formBasicText"
				validationState={this.state.validMsg}
			>
				<ControlLabel>{this.state.title}</ControlLabel>
				<input
					disabled={this.props.require}
					className="form-control"
					type={this.state.type}
					value={this.state.value}
					placeholder={this.state.title}
					onChange={this.handleChange.bind(this)}
					name={this.state.title}
				/>
				<FormControl.Feedback />
				<HelpBlock>{this.getErrorMessage()}</HelpBlock>
			</FormGroup>
		);
	}
}


export class ValidationRadio extends ValidationInput {
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			value: '',
			type: props.type,
			isRequire: props.require,
			rule: props.rule,
			solution: ''
		};
		this.handleChange = this.handleChange.bind(this);

	}

	getErrorMessage() {
		if (this.getValidationState() === '') return null;
		if (this.getValidationState()==='success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}

	getValidationState() {
		if(!this.state.isRequire) return null;
		return this.state.validMsg;
	}

	getValidationMessage(addr,val) {
		const len = val.length;
		if (len === 0) return null;
		if (val.indexOf('s') > val.indexOf('a') > val.indexOf('p') || val.indexOf('l') > val.indexOf('i') > val.indexOf('a') > val.indexOf('f') ) {
			return 'success';
		}
		return 'error';
	}

	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success'){
			if(this.props.tag!=undefined) 
				this.props.handleData(this.props.tag,nextState.value,nextState.validMsg==='success');
			else
				this.props.handleData(nextState.type,nextState.value,nextState.validMsg==='success');
		}

	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}

	handleChange(event) {
		let addr = event.target.name;
		let val = event.target.value;
		this.setState({
			value: val,
			validMsg: this.getValidationMessage(addr,val)
		});

	}


	
	requireSolution() {
		let tmp = this.state.value;
		let res = !(tmp.indexOf('l') > tmp.indexOf('i') > tmp.indexOf('a') > tmp.indexOf('f'));
		console.log('Res :'+res);
		return  res;
	}

	render() {

		if(this.state.title === 'status'){

			console.log('TAG :'+this.props.tag);

			if(this.props.formname === 'RiskEvaluation'){
				return (
					<div>
						<Col md={2}>
							<FormGroup
								ref="form"
								controlId="formBasicText"
								validationState={this.getValidationState()}
							>
								<div>
									<div>
										<ControlLabel></ControlLabel>
									</div>
									<div>
										<fieldset id={this.props.tag}>
											<Col md={6}><Radio name={this.props.tag} value={'pass'+this.props.tag} onChange={this.handleChange}></Radio></Col>
											<Col md={6}><Radio name={this.props.tag} value={'fail'+this.props.tag} ref='isFail' onChange={this.handleChange}></Radio></Col>
										</fieldset>
									</div>
								</div>				
								<FormControl.Feedback />
								<HelpBlock>{this.getErrorMessage()}</HelpBlock>
							</FormGroup>
						</Col>
						<Col md={2}><ValidationInput require={this.requireSolution()} type="text"/></Col>
					</div>

				);
			}

			return (
				<div>
					<FormGroup
						ref="form"
						controlId="formBasicText"
						validationState={this.getValidationState()}
					>
						<div>
							<div>
								<ControlLabel></ControlLabel>
							</div>
							<div>
								<fieldset id={this.props.tag}>
									<Col md={6}><Radio name={this.props.tag} value={'pass'+this.props.tag} onChange={this.handleChange}></Radio></Col>
									<Col md={6}><Radio name={this.props.tag} value={'fail'+this.props.tag} onChange={this.handleChange}></Radio></Col>
								</fieldset>
							</div>
						</div>				
						<FormControl.Feedback />
						<HelpBlock>{this.getErrorMessage()}</HelpBlock>
					</FormGroup>
				</div>
		
			);

		}

		return (
			<div>
				<FormGroup
					ref="form"
					controlId="formBasicText"
					validationState={this.getValidationState()}
				>
					<div>
						<div>
							<ControlLabel>{this.state.title}</ControlLabel>
						</div>
						<fieldset id="user-type">
							<Radio name="user-type" inline value="company" onChange={this.handleChange}>
							บริษัท
							</Radio>
							{' '}
							<Radio name="user-type" inline value="corporation" onChange={this.handleChange}>
							ห้างหุ้นส่วนจำกัด
							</Radio>
							{' '}
							<Radio name="user-type" inline value="community" onChange={this.handleChange}>
							วิสาหกิจชุมชน
							</Radio>
							{' '}
							<Radio name="user-type" inline value="person" onChange={this.handleChange}>
							บุคคลธรรมดา
							</Radio>
						</fieldset>
					</div>				
					<FormControl.Feedback />
					<HelpBlock>{this.getErrorMessage()}</HelpBlock>
				</FormGroup>
			</div>
			
		);
	}
}







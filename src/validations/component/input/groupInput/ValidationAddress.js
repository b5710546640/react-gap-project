import React from 'react';
import { HelpBlock, FormGroup, FormControl, ControlLabel, Row, Col } from 'react-bootstrap';
import { ValidationInput } from '../ValidationInput.js';

export default class ValidationAddress extends ValidationInput {
    
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			address: '',
			district: '',
			road: '',
			amphoe: '',
			province: '',
			zipcode: '',
			type: 'text',
			isRequire: props.require,
			rule: props.rule,
			confirmPassword: ''
		};
    
		this.handleChange = this.handleChange.bind(this);
	}
    
	getErrorMessage() {
		if (this.getValidationState() === '') return null;
		if (this.getValidationState() === 'success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}
    
	getValidationState() {
		return this.state.validMsg;
	}

	getValidationMessage(addr,val) {
		let address = this.state.address;
		if(addr === 'address'){
			address = val;
		}
		let amphoe = this.state.amphoe;
		if(addr === 'amphoe'){
			amphoe = val;
		}
		let province = this.state.province;
		if(addr === 'province'){
			province = val;
		}
		let zipcode = this.state.zipcode;
		if(addr === 'zipcode'){
			zipcode = val;
		}
		console.log(address.length === 0 && amphoe.length === 0 && province.length === 0 && zipcode.length === 0);
		if (address.length === 0 && amphoe.length === 0 && province.length === 0 && zipcode.length === 0) return null;
		if (address.length > 0 && amphoe.length > 0 && province.length > 2 && zipcode.length === 5) {
			return 'success';
		}
		return 'error';
	}

	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success'){
			this.props.handleData("address",nextState.address,nextState.validMsg==='success');
			this.props.handleData("road",nextState.road,nextState.validMsg==='success');
			this.props.handleData("district",nextState.district,nextState.validMsg==='success');
			this.props.handleData("amphoe",nextState.amphoe,nextState.validMsg==='success');
			this.props.handleData("province",nextState.province,nextState.validMsg==='success');
			this.props.handleData("zipcode",nextState.zipcode,nextState.validMsg==='success');
		}
	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}
    
	handleChange(event) {
		let addr = event.target.name;
		let val = event.target.value;
		this.setState({
			[addr]: val,
			validMsg: this.getValidationMessage(addr,val)
		});
	}
    
    
	render() {
		return (
			<FormGroup
				ref="form"
				controlId="formBasicText"
				validationState={this.getValidationState()}
			>
				<ControlLabel>{this.state.title}</ControlLabel>
				<Row className="show-grid">
					<Col md={2}>
						<input
							id="address"
							className="form-control"
							type={this.state.type}
							value={this.state.address}
							placeholder="บ้านเลขที่"
							onChange={this.handleChange}
							name="address"
						/>
					</Col>
					<Col md={2}>
						<input
							id="road"
							className="form-control"
							type={this.state.type}
							value={this.state.road}
							placeholder="ถนน"
							onChange={this.handleChange}
							name="road"
						/>
					</Col>
					<Col md={2}>
						<input
							id="district"
							className="form-control"
							type={this.state.type}
							value={this.state.district}
							placeholder="ตำบล"
							onChange={this.handleChange}
							name="district"
						/>
					</Col>
					<Col md={2}>
						<input
							id="amphoe"
							className="form-control"
							type={this.state.type}
							value={this.state.amphoe}
							placeholder="อำเภอ"
							onChange={this.handleChange}
							name="amphoe"
						/>
					</Col>
					<Col md={2}>
						<input
							id="province"
							className="form-control"
							type={this.state.type}
							value={this.state.province}
							placeholder="จังหวัด"
							onChange={this.handleChange}
							name="province"
						/>
					</Col>
					<Col md={2}>
						<input
							id="zipcode"
							className="form-control"
							type={this.state.type}
							value={this.state.zipcode}
							placeholder="รหัสไปรษณีย์"
							onChange={this.handleChange}
							name="zipcode"
						/>
					</Col>
				</Row>
    
				<FormControl.Feedback />
				<HelpBlock>{this.getErrorMessage()}</HelpBlock>
			</FormGroup>
		);
	}
    
}
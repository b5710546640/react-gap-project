import React from 'react';
import { Button, Table} from 'react-bootstrap';
import MyLargeModal from './ValidationModal.js';
import { ValidationInput } from '../ValidationInput.js';

export default class ValidationFarmField extends ValidationInput {
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			value: '',
			type: props.type,
			isRequire: props.require,
			rule: props.rule,
			lgShow: false,
			size: 0
		};
		this.toggleModal = this.toggleModal.bind(this);
	}

	getErrorMessage() {
		if (this.getValidationState() === '') return null;
		if (this.getValidationState()==='success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}

	getValidationState() {
		return null;
	}


	toggleModal() {
		if(this.state.lgShow)
			this.setState({ lgShow: !this.state.lgShow, size: (this.state.size+1)	});
		else this.setState({ lgShow: !this.state.lgShow	});
	}

	render() {

		if(this.state.size>0 && !this.state.lgShow){
			return (
				<Table striped bordered condensed hover>
					<thead>
						<tr>
							<th>
								<Button type="button" onClick={this.toggleModal}>
									+
								</Button>
							</th>
							<th>ชนิดพืช</th>
							<th>ที่อยู่แปลง</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>มะม่วง,ปาล์ม</td>
							<td>111 ถนน A จังหวัด B 10222</td>
						</tr>
						<tr>
							<td>2</td>
							<td>ข้าว</td>
							<td>112 ถนน A จังหวัด B 10222</td>
						</tr>
					</tbody>
				</Table>
			);
		}

		if(!this.state.lgShow){
			return (
				<Button type="button" onClick={this.toggleModal}>
					เพิ่มแปลงที่ต้องการขอใบรับรอง
				</Button>
			);
		}

		return (

			<MyLargeModal show={this.state.lgShow} handleData={this.props.handleData}
				onClose={this.toggleModal}>
					Here's some content for the modal
			</MyLargeModal>
			

	
		);
	}
}


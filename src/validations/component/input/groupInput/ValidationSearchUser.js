import React from 'react';
import { Button, Table} from 'react-bootstrap';
import MyLargeModal from './ValidationModal.js';

import { ValidationInput, ValidationRadio } from '../../../component/input/ValidationInput.js';
import { HelpBlock, FormGroup, FormControl, ControlLabel, Radio,Row, Col } from 'react-bootstrap';


export default class ValidationSearchUser extends ValidationInput {
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			value: '',
			type: props.type,
			isRequire: props.require,
			rule: props.rule,
			lgShow: false,
			size: 0,
			validMsg: null
		};
	}

	getErrorMessage() {
		if (this.getValidationState() === '') return null;
		if (this.getValidationState()==='success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}
    
	getValidationState() {
		const len = this.state.value.length;
		if (len != 13) return null;
		if (this.state.value === '1234567890123') {
			this.setState({ lgShow: true});
			return 'success';
		}
		return 'error';
	}


	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success')
			this.props.handleData("personalID",nextState.value,nextState.validMsg==='success');
	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}
	
	handleChange(event) {
		let val = event.target.value;
		this.setState({ value: val });
		this.validateState();
	}




	render() {

		console.log('lgshow :'+this.state.lgShow);

		if(!this.state.lgShow){

			// console.log('ERROR :'+(this.getValidationState()==='error'));

			if(this.getValidationState() === 'error'){
				window.location.href = '../register';
			}

			return (
				<FormGroup
					ref="form"
					controlId="formBasicText"
					validationState={this.state.validMsg}
                    
				>
					<ControlLabel>{this.state.title}</ControlLabel>
					<input
						className="form-control"
						type={this.state.type}
						value={this.state.value}
						placeholder={this.state.title}
						onChange={this.handleChange.bind(this)}
						name={this.state.title}
					/>
					<FormControl.Feedback />
					<HelpBlock>{this.getErrorMessage()}</HelpBlock>
				</FormGroup>
			);
		}

		console.log('ToggleModal')

		return (

			<MyLargeModal show={true} isSearchUser={true}>
					Here's some content for the modal
			</MyLargeModal>
	
		);
	}
}


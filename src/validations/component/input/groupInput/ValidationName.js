import React from 'react';
import { HelpBlock, FormGroup, FormControl, ControlLabel, Row, Col } from 'react-bootstrap';
import { ValidationInput } from '../ValidationInput.js';
import { SelectedInput } from '../SelectedInput';

export default class ValidationName extends ValidationInput {
    
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			prefix_name: '',
			firstname: '',
			lastname: '',
			type: 'text',
			isRequire: props.require,
			rule: props.rule,
			confirmPassword: ''
		};
		this.handleChange = this.handleChange.bind(this);
	}
    
	getErrorMessage() {
		if (this.state.validMsg === '') return null;
		if (this.state.validMsg === 'success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}
    
	getValidationState() {
		return this.state.validMsg;
	}

	getValidationMessage(addr, val) {
		let firstname = this.state.firstname;
		if (addr == 'firstname') {
			firstname = val;
		}
		let lastname = this.state.lastname;
		if (addr == 'lastname') {
			lastname = val;
		}
		if (firstname === 0 && lastname === 0) return null;
		if (firstname.length >= 3 && lastname.length >= 3) {
			return 'success';
		}
		return 'error';
	}

	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success'){
			this.props.handleData("firstname",nextState.firstname,nextState.validMsg==='success');
			this.props.handleData("lastname",nextState.lastname,nextState.validMsg==='success');
			this.props.handleData("prefix_name",nextState.prefix_name,nextState.validMsg==='success');
		}
	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}
    
	handleChange(event) {
		let addr = event.target.name;
		let val = event.target.value;
		console.log("handleChange",addr,val);
		this.setState({
			[addr]: val,
			validMsg: this.getValidationMessage(addr,val)
		});
		
	}
    
    
	render() {
		return (
			<FormGroup
				ref="form"
				controlId="formBasicText"
				validationState={this.getValidationState()}
			>
				<ControlLabel>{this.state.title}</ControlLabel>
				<Row className="show-grid">
					<Col md={2}>
						<SelectedInput option={["mr","mrs","ms"]} />
					</Col>
					<Col md={5}>
						<input
							className="form-control"
							type={this.state.type}
							value={this.state.firstname}
							placeholder="ชื่อจริง"
							onChange={this.handleChange}
							name="firstname"
						/>
					</Col>
					<Col md={5}>
						<input
							className="form-control"
							type={this.state.type}
							value={this.state.lastname}
							placeholder="นามสกุล"
							onChange={this.handleChange}
							name="lastname"
						/>
					</Col>
				</Row>
    
				<FormControl.Feedback />
				<HelpBlock>{this.getErrorMessage()}</HelpBlock>
			</FormGroup>
		);
	}
    
}
    
import React from 'react';
import { HelpBlock, FormGroup, FormControl, ControlLabel, Row, Col} from 'react-bootstrap';
import { ValidationInput } from '../ValidationInput.js';

export default class ValidationConfirmPassword extends ValidationInput {
    
	constructor(props) {
		super(props);
		this.state = {
			title: props.name,
			password: '',
			type: 'password',
			isRequire: props.require,
			rule: props.rule,
			confirmPassword: ''
		};
		this.handleChange = this.handleChange.bind(this);
	}
    
	getErrorMessage() {
		if (this.getValidationState() === '') return null;
		if (this.getValidationState() === 'success')
			return 'ข้อมูลถูกต้อง';
		return this.state.rule;
	}
    
    
	getValidationState() {
		const len = this.state.password.length;
		if (len === 0) return null;
		if (this.state.password === this.state.confirmPassword && len >= 5) {
			return 'success';
		}
		return 'error';
	}

	getValidationMessage(addr, val) {
		let password = this.state.password;
		if (addr == 'password') {
			password = val;
		}
		const len = password.length;
		let confirmPassword = this.state.confirmPassword;
		if (addr == 'confirmPassword') {
			confirmPassword = val;
		}
		if (len === 0) return null;
		if (password === confirmPassword && len >= 5) {
			return 'success';
		}
		return 'error';
	}

	validateState(){
		let re = this.getValidationState();
		this.setState({ validMsg: re });
	}

	handleData(nextState){
		if(nextState.validMsg==='success'){
			this.props.handleData("password",nextState.password,nextState.validMsg==='success');
		}
	}

	componentWillUpdate(nextProps, nextState) {
		if (this.state != nextState)
			this.handleData(nextState);
	}
    
	handleChange(event) {
		let addr = event.target.name;
		let val = event.target.value;
		this.setState({
			[addr]: val,
			validMsg: this.getValidationMessage(addr,val)
		});
	}
    
	render() {
		return (
			<FormGroup
				controlId="formBasicText"
				validationState={this.getValidationState()}
			>
				<ControlLabel>{this.state.title}</ControlLabel>
				<Row className="show-grid">
					<Col md={6}>
						<input
							className="form-control"
							type={this.state.type}
							value={this.state.password}
							placeholder="รหัสผ่านอย่างน้อย 8 ตัว"
							onChange={this.handleChange}
							name="password"
						/>
					</Col>
					<Col md={6}>
						<input
							className="form-control"
							type={this.state.type}
							value={this.state.confirmPassword}
							placeholder="ยืนยันรหัสผ่าน"
							onChange={this.handleChange}
							name="confirmPassword"
						/>
					</Col>
				</Row>
                   
				<FormControl.Feedback />
				<HelpBlock>{this.getErrorMessage()}</HelpBlock>
			</FormGroup>
		);
	}
    
}
    
    
import React from 'react';
import {  BrowserRouter as Router,
	Route,
	Link } from 'react-router-dom';
import { Button, Panel} from 'react-bootstrap';
import { ValidationInput } from '../ValidationInput.js';
import ValidationAddress from './ValidationAddress.js';
import { FormRegister } from '../../../../comps/RegisterContent.js';
import Validation from '../../../Validation.js';

export default class MyLargeModal extends React.Component {
	render() {
		console.log('require :'+this.props.requireRegister);
		if(!this.props.show) {
			return null;
		}

		if(this.props.isSearchUser){
			return (
				<div className="backdrop">
					<ValidationInput handleData={this.props.handleData} name='ชื่อ-สกุล' type='name' />
					<ValidationInput handleData={this.props.handleData} name='เลขประจำตัวประชาชน' type='text' />
					<ValidationAddress handleData={this.props.handleData} type="address" name='ที่อยู่' />
				</div>
								
			);
			
		}
	
		return (
			<Panel>
				<div className="backdrop">
					<ValidationInput handleData={this.props.handleData} type="text" name="ชนิดพืช" />
					<ValidationAddress handleData={this.props.handleData} type="address" name="ที่อยู่แปลง" />
					<Button onClick={this.props.onClose}>Submit</Button>
				</div>
			</Panel>
			
		);
	}
}
  

